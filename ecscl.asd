(defsystem "ecscl"
  :description "ecscl: A library for implementing ECS systems in Common Lisp."
  :author "_venko"
  :licence "WTFPL"
  :components ((:module "src"
                :serial t
                :components ((:file "package")
                             (:file "hibits")
                             (:file "world")
                             (:file "ecs")))))
