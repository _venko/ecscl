;;;; world.lisp

(defpackage ecscl.world
  (:use #:cl
        #:ecscl.arena)
  (:import-from #:ecscl.arena
                #:make-arena)
  (:export #:world
           #:register-component))

(in-package #:ecscl.world)

(defstruct world
  "Struct for containing all components and relevant resources."
  (components (make-hash-table))
  (entities (make-arena)))

(defun register-component (world tag)
  "Registers a component with the given tag in the world. Throws an error if the
given tag is already registered."
  (with-slots (components) world
    (when (not (null (gethash tag components)))
      (error (format nil "Component with tag '~a' already registered." tag)))
    (setf (gethash tag components) (make-arena))))
