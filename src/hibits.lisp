;;;; hibits.lisp
;;;;
;;;; This file contains an implementation of a "hierarchical bit set" structure,
;;;; or "hibitset" for short. A hibitset is a structure used for efficiently
;;;; tracking set membership. Internally, a hibitset is represented as a number
;;;; of bit arrays, each representing a hierarchy in the set. At the lowest
;;;; level of the hierarchy, one bit represents one set element. Each higher
;;;; level represents a block of bits in the next lower level.
;;;;
;;;; An illustration of a hypothetical hibitset can be seen below:
;;;; Layer 3: 1------------------------------------------------ ...
;;;; Layer 2: 1------------------ 1------------------ 0-------- ...
;;;; Layer 1: 1--- 0--- 0--- 0--- 1--- 0--- 1--- 0--- 0--- 0--- ...
;;;; Layer 0: 0010 0000 0000 0000 0011 0000 1111 0000 0000 0000 ...

(defpackage ecscl.hibits
  (:use #:cl)
  (:shadow :remove)
  (:export #:bitset
           #:make-bitset
           #:extend
           #:make-bitset-with-capacity
           #:clear
           #:insert
           #:remove
           #:make-iterator))

(in-package #:ecscl.hibits)

(defconstant +bits+ 6)

(defconstant +layers+ 4)
(defconstant +max+ (* +bits+ +layers+))

(defconstant +max-eid+ (ash 2 (- +max+ 1)))

(defconstant +shift-0+ 0)
(defconstant +shift-1+ (+ +shift-0+ +bits+))
(defconstant +shift-2+ (+ +shift-1+ +bits+))
(defconstant +shift-3+ (+ +shift-2+ +bits+))

(defun row (index shift)
  "Gets the location of a bit in the row."
  (logand (ash index (- shift)) (- (ash 1 +bits+) 1)))

(defun offset (index shift)
  "Index of the row that the bit is in."
  (multiple-value-bind (quot rem) (floor index (ash 1 shift))
    quot))

(defun offsets (index)
  "Helper function for getting all of the offsets of an index at once."
  (list (offset index +shift-1+)
        (offset index +shift-2+)
        (offset index +shift-3+)))

(defun mask (index shift)
  "Bitmask of the row that the bit is in."
  (ash 1 (row index shift)))

(defun init-bitset-layer ()
  "Helper function for initializing a layer in a bitset"
  (make-array 0
              :element-type '(unsigned-byte 64)
              :initial-element 0
              :adjustable t))

(defstruct bitset
  "A hierarchical bit set."
  (l3 0 :type (unsigned-byte 64))
  (l2 (init-bitset-layer))
  (l1 (init-bitset-layer))
  (l0 (init-bitset-layer)))

(defun error-if-invalid-range (index)
  "Throws an error when the given index is invalid."
  (unless (<= 0 index +max-eid+)
    (error (format t "Expected index to be in range [0, ~a], found ~a" +max-eid+ index))))

(defun extend-layer (layer capacity)
  "Extends a layer of a bitset to have the given capacity."
  (when (<= (array-total-size layer) capacity)
    (adjust-array layer (+ capacity 1) :initial-element 0)))

(defun extend (bitset capacity)
  "Extends a bitset to have the given capacity."
  (error-if-invalid-range capacity)
  (with-slots (l2 l1 l0) bitset
    (destructuring-bind (p0 p1 p2) (offsets capacity)
      (extend-layer l2 p2)
      (extend-layer l1 p1)
      (extend-layer l0 p0))))

(defun make-bitset-with-capacity (capacity)
  "Initializes a bitset with the given capacity."
  (let ((set (make-bitset)))
    (extend set capacity)
    set))

(defun get-from-layer (bitset layer index)
  (with-slots (l3 l2 l1 l0) bitset
    (case layer
      (0 (aref l0 index))
      (1 (aref l1 index))
      (2 (aref l2 index))
      (3 l3)
      (otherwise (error "Expected number in range [0, 3].")))))

(defun clear (bitset)
  "Clears a bitset, deallocating back down to minimal size."
  (with-slots (l3 l2 l1 l0) bitset
    (setf l3 0)
    (adjust-array l2 0)
    (adjust-array l1 0)
    (adjust-array l0 0))
  nil)

(defun insert (bitset index)
  "Inserts the given index into a bitset. Returns true if the index was already
set, otherwise nil."
  (with-slots (l3 l2 l1 l0) bitset
    (destructuring-bind (p0 p1 p2) (offsets index)
      (when (<= (array-total-size l0) p0)
        (extend bitset index))

      (let ((mask0 (mask index +shift-0+))
            (old0 (aref l0 p0)))
        (setf (aref l0 p0) (logior old0 mask0))

        (when (not (= (logand old0 mask0) 0))
          (return-from insert t))

        (let ((mask1 (mask index +shift-1+))
              (mask2 (mask index +shift-2+))
              (mask3 (mask index +shift-3+))
              (old1 (aref l1 p1))
              (old2 (aref l2 p2)))
            (setf (aref l1 p1) (logior old1 mask1)
                  (aref l2 p2) (logior old2 mask2)
                  l3 (logior l3 mask3))
            nil)))))

(defun remove (bitset index)
  "Removes the given index from a bitset. Returns true if the index was removed
successfully and returns nil if the index did not exist in the first place."
  (defun unset-layer-bit (layer position shift)
    (setf (aref layer position) (logand (aref layer position) (lognot (mask index shift)))))

  (defun chunk-zerop (layer position)
    (zerop (aref layer position)))

  (with-slots (l3 l2 l1 l0) bitset
    (destructuring-bind (p0 p1 p2) (offsets index)
      (when (>= p0 (array-total-size l0))
        (return-from remove nil))

      (when (= (logand (aref l0 p0) (mask index +shift-0+)))
        (return-from remove nil))

      (unset-layer-bit l0 p0 +shift-0+)
      (unless (chunk-zerop l0 p0)
        (return-from remove t))

      (unset-layer-bit l1 p1 +shift-1+)
      (unless (chunk-zerop l1 p1)
        (return-from remove t))

      (unset-layer-bit l2 p2 +shift-2+)
      (unless (chunk-zerop l2 p2)
        (return-from remove t))

      (setf l3 (lognot (mask index +shift-3+)))

      t)))

(defun containsp (bitset index)
  "Checks if the given index exists in a bitset."
  (let ((p0 (offset index +shift-1+))
        (l0 (bitset-l0 bitset)))
    (and (< p0 (array-total-length l0))
         (not (zerop (logand (aref l0 p0) (mask index +shift-0+)))))))

(defstruct (bitset-iterator (:constructor nil))
  bitset masks prefix)

(defun make-bitset-iterator (source-bitset)
  (let ((iterator (allocate-instance (find-class 'bitset-iterator))))
    (with-slots (bitset masks prefix) iterator
      (setf bitset source-bitset
            masks (make-array +layers+
                              :element-type '(unsigned-byte 64)
                              :initial-contents (list 0 0 0 (bitset-l3 source-bitset)))
            prefix (make-array (- +layers+ 1)
                               :element-type '(unsigned-byte 32)
                               :initial-element 0))
      iterator)))

(defun trailing-zeros (x)
  "Returns the number of trailing zeros in the binary representation of an
unsigned, 64-bit number"
  (let ((int-size (expt 2 +bits+)))
    (declare (type (unsigned-byte int-size) x))
    (if (= x 0) int-size
        (loop :for shift = 0 :then (+ shift 1)
              :for mask = (ash 1 shift)
              :while (= (logand x mask) 0)
              :finally (return shift)))))

(defun handle-level (iterator level)
  "Helper function for choosing how to iterate over a single layer. Returns
'empty if the layer is empty, 'continue if iteration needs to continue on the
layer below, or an integer representing an index."
  (with-slots (bitset masks prefix) iterator
    (when (zerop (aref masks level))
      (return-from handle-level 'empty))

    ;; Find the first bit that isnt zero and its index
    (let* ((first-bit (trailing-zeros (aref masks level)))
           (safe-prefix (if (< level (- +layers+ 1))
                            (aref prefix level)
                            0))
           (index (logior safe-prefix first-bit)))
      ;; Remove the first non-zero bit from the mask
      (setf (aref masks level) (logand (aref masks level) (lognot (ash 1 first-bit))))
      (if (zerop level)
          index ; We're at the lowest level so `index' is the next set bit
          (progn
            ;; Take the corresponding chunk from the layer below
            (decf level)
            (setf (aref masks level) (get-from-layer bitset level index)
                  (aref prefix level) (ash index +bits+))
            'continue)))))

(defun make-iterator (bitset)
  "Creates a function for iterating over a bitset. Returns nil when there are no
more elements in the bitset."
  (let ((iter-state (make-bitset-iterator bitset)))
    (lambda ()
      (loop :named top-loop :do
        (block inner-loop
          (loop :for level :from 0 :below +layers+
                :for result = (handle-level iter-state level)
                :when (numberp result) :do (return-from top-loop result)
                :when (eq result 'continue) :do (return-from inner-loop)
                :finally (return-from top-loop nil)))))))
